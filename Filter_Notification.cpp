#include "libMADS.h"
#include <stdio.h>

int main(){
    int i = 0;

    FilterList *filterList = FilterList_create();

    DataList *dataList = DataList_create();

    while (i<10) {
        printf("%i\n", i);

        DataModel *notification1 = dataModel_create();
        dataModel_addAttributeINT64(notification1,"SensorID",25);
        dataModel_addAttributeINT32(notification1,"Temperature",12);
         DataModel *notification2 = dataModel_create();
        dataModel_addAttributeINT64(notification2,"SensorID",27);
        dataModel_addAttributeINT32(notification2,"Temperature",15);
         DataModel *notification3 = dataModel_create();
        dataModel_addAttributeINT64(notification3,"SensorID",29);
        dataModel_addAttributeINT32(notification3,"Temperature",19);

        Filter *filter1 = Filter_create();
        Filter_addSubFilterINT64(filter1, "SensorID", GREATER_EQUAL, 25);
         Filter *filter2 = Filter_create();
        Filter_addSubFilterINT64(filter2, "SensorID", GREATER_EQUAL, 27);
         Filter *filter3 = Filter_create();
        Filter_addSubFilterINT64(filter3, "SensorID", GREATER_EQUAL, 30);

        FilterList_addFilter(filterList, filter1);
        FilterList_addFilter(filterList, filter2);
        FilterList_addFilter(filterList, filter3);
        DataList_addData(dataList, notification1);
         DataList_addData(dataList, notification2);
          DataList_addData(dataList, notification3);

        i++;
    }

    printf("%li \n", attribute_getValueINT64(TestDataModel, "Hallo World"));
    printf("%i \n", attribute_getValueINT32(TestDataModel, "Temperature"));
}